import React, { useState, useEffect } from 'react';

function HatForm() {
    const [locations, setLocations] = useState([]);
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [location, setLocation] = useState('');

    const handleFabricChange = (event) => {
      const value = event.target.value;
      setFabric(value)
    }
    const handleStyleChange = (event) => {
      const value = event.target.value;
      setStyle(value)
    }
    const handleColorChange = (event) => {
      const value = event.target.value;
      setColor(value)
    }
    const handleLocationChange = (event) => {
      const value = event.target.value;
      setLocation(value)
    }

    const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {}
      data["fabric"] = fabric;
      data["style"] = style;
      data["color"] = color;
      data["location"] = location;
      console.log(data);

      const hatUrl = 'http://localhost:8090/api/hats/';
      const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(hatUrl, fetchConfig);
      if (response.ok) {
        console.log('posted')
        const newHat = await response.json();
        console.log(newHat);

        setFabric('');
        setStyle('');
        setColor('');
        setLocation('');
      }
    }

    const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
      }
    }

    useEffect(() => {
      fetchData();
    }, []);

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={fabric}/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStyleChange} placeholder='Style' required type="text" name="style" id="style" className="form-control" value={style}/>
                <label htmlFor="style">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} placeholder='Color' required type="text" name="color" id="color" className="form-control" value={color}/>
                <label htmlFor="color">Color</label>
              </div>
              <div>
                <select onChange={handleLocationChange} required id="location" name="location" className="form-select mb-3">
                  <option key={location.id} value={location.id}>Location</option>
                  {locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}> Location: {location.id}, Closet: {location.closet_name}, Section: {location.section_number}, Shelf: {location.shelf_number}
                      </option>
                      );
                  })}
                </select>
              </div>
              <div>
                <button className="btn btn-primary" type='submit'>Create</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
}

export default HatForm;
