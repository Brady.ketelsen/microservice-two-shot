import React, { useEffect, useState} from "react";

function HatList(props) {
  const [hats, setHats] = useState([]);

  async function loadHats() {
  const response = await fetch("http://localhost:8090/api/shoes/")
  if (response.ok) {
    const data = await response.json();
    setHats(data);
  }
  }

  useEffect(() => {
    loadHats();
  }, [])

  const deleteHat = async (event) => {
      console.log('click')
      const hatId = event.target.value
      console.log(event.target)

      const hatUrl = `http://localhost:8090/api/hats/${hatId}/`;
      const fetchConfig = {
        method: "DELETE",
      };

      const response = await fetch(hatUrl, fetchConfig);
      if (response.ok) {
        console.log('deleted')
        location.reload();
      }
    }

  return (
    <div>

    <div>
      <table className="table table-striped">
      <thead>
        <tr>
          <th>Fabric</th>
          <th>Style</th>
          <th>Color</th>
          <th>Location</th>
          <th>Picture Url</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {props.hats.map(hat => {
          return (
            <tr>
              <td>{ hat.fabric }</td>
              <td>{ hat.style }</td>
              <td>{ hat.color }</td>
              <td>{ hat.location.closet_name }, Sect: { hat.location.section_number }, Shelf: { hat.location.shelf_number }</td>
              <td><a href={ hat.picture_url }>Pic</a></td>
              <td><button onClick={deleteHat} className=
              "btn btn-danger" id="delete" value={hat.id}>Delete</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </div>
    </div>
  )
}

export default HatList;
