from django.db import models

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=150, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

class Hat(models.Model):
    fabric = models.CharField(max_length=150)
    style = models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    picture_url = models.URLField(null=True, blank=True)

    location = models.ForeignKey(
        "LocationVO",
        related_name="hats",
        on_delete=models.CASCADE,
    )
