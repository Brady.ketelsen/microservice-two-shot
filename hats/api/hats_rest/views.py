from django.shortcuts import render, HttpResponse
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from common.json import ModelEncoder
import json

from .models import Hat, LocationVO
# from wardrobe_api.models import Location

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]

class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder()
    }

class HatsDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder()
    }

# Create your views here.

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatsListEncoder, safe=False,)
    else: #POST
        content = json.loads(request.body)
        try:
            # location_data = content["location"]
            # closet_name = location_data["closet_name"]
            # section_number = location_data["section_number"]
            # shelf_number = location_data["shelf_number"]
            # content["location"] = {"closet_name": closet_name, "section_number": section_number, "shelf_number": shelf_number}
            location_id = content["location"]
            location = LocationVO.objects.get(pk=location_id)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid location ID"}, status=400)
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hats(request, id):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder=HatsDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Invalid hat ID"}, status=400)
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else: #PUT
        content = json.loads(request.body)
        Hat.objects.filter(id=id).update(**content)
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False
        )

def api_list_location_vo(request):
    vos = LocationVO.objects.all()
    return JsonResponse({"vos": vos}, encoder=LocationVODetailEncoder, safe=False)
